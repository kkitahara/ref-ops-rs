# Change log

## v0.2.5 - 2023.10.06
* Updated `categories` field in `Cargo.toml`.
* Updated rustdoc.

## v0.2.4 - 2023.10.02
* Updated doctests.

## v0.2.3 - 2023.10.01
* Added `documentation` field to `Cargo.toml`.

## v0.2.2 - 2023.10.01
* Added unit tests.

## v0.2.1 - 2022.11.16
* Updated rustdoc.

## v0.2.0 - 2022.11.11
* Extracted from `as-is` crate.
