#![no_std]

//! An escape hatch for implementing `ops` traits for references to newtypes.
//!
//! As of Rust 1.73.0, the following code does not compile:
//! ```compile_fail
//! use core::ops::Neg;
//!
//! struct A<T>(T);
//!
//! impl<'a, T> Neg for &'a A<T>
//! where
//!     &'a T: Neg,
//! {
//!     type Output = A<<&'a T as Neg>::Output>;
//!
//!     fn neg(self) -> Self::Output {
//!         A(-&self.0)
//!     }
//! }
//!
//! fn f<T>(a: T)
//! where
//!     for<'a> &'a T: Neg,
//! {
//!     let minus_a = -&a;
//!
//!     // to do something with `a` and `minus_a`
//! }
//!
//! fn g<T>(a: T)
//! where
//!     for<'a> &'a T: Neg,
//! {
//!     f(a);
//! }
//! ```
//! but the following code does:
//! ```
//! use core::ops::Neg;
//! use ref_ops::RefNeg;
//!
//! struct A<T>(T);
//!
//! impl<T> Neg for &A<T>
//! where
//!     T: RefNeg,
//! {
//!     type Output = A<T::Output>;
//!
//!     fn neg(self) -> Self::Output {
//!         A(self.0.ref_neg())
//!     }
//! }
//!
//! fn f<T>(a: T)
//! where
//!     for<'a> &'a T: Neg,
//! {
//!     let minus_a = -&a;
//!
//!     // to do something with `a` and `minus_a`
//! }
//!
//! fn g<T>(a: T)
//! where
//!     for<'a> &'a T: Neg,
//! {
//!     f(a);
//! }
//! ```

mod ref_unary;
pub use ref_unary::*;

mod ref_mut_unary;
pub use ref_mut_unary::*;

mod ref_binary;
pub use ref_binary::*;

mod ref_mut_binary;
pub use ref_mut_binary::*;
