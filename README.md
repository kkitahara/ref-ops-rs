[![crates.io](https://img.shields.io/crates/v/ref-ops)](https://crates.io/crates/ref-ops)
[![docs.rs](https://img.shields.io/docsrs/ref-ops)](https://docs.rs/ref-ops)
[![pipeline status](https://gitlab.com/kkitahara/ref-ops-rs/badges/main/pipeline.svg)](https://gitlab.com/kkitahara/ref-ops-rs/-/pipelines)
[![coverage report](https://gitlab.com/kkitahara/ref-ops-rs/badges/main/coverage.svg)](https://kkitahara.gitlab.io/ref-ops-rs/coverage/)

# RefOps

An escape hatch for implementing `ops` traits for references to newtypes.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

---

&copy; 2021&ndash;2023 Koichi Kitahara
